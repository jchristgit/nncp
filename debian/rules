#!/usr/bin/make -f

export PREFIX = /usr

%:
	dh $@ --builddirectory=_build --buildsystem=golang --with=golang

override_dh_auto_install:
	dh_auto_install -- --no-source

# Fix for the problem of the files being in nncp/v7/src but not looking for
# them there.
override_dh_auto_build:
	#mkdir -p _build/src/go.cypherpunks.ru/nncp/v7
	#cp -r src/* _build/src/go.cypherpunks.ru/nncp/v7/
	#ln -s src/uilive _build/src/go.cypherpunks.ru/nncp/v7/uilive
	mv -vi _build/src/go.cypherpunks.ru/nncp/v7/src/* _build/src/go.cypherpunks.ru/nncp/v7/
	ln -s v7 _build/src/go.cypherpunks.ru/nncp/v8
	dh_auto_build
	pandoc --standalone --to man debian/nncp.1.md -o debian/nncp.1
	cd doc && plantuml -tutxt -pipe < sp.plantuml > sp.plantuml.txt
	cd doc && makeinfo -D "VERSION `perl -ne 'print "$$1\n" if /Version.* = "(.*)"$$/' < ../src/nncp.go`" \
	   	--set-customization-variable SECTION_NAME_IN_TITLE=1 \
		--set-customization-variable TREE_TRANSFORMATIONS=complete_tree_nodes_menus \
		--set-customization-variable CLOSE_QUOTE_SYMBOL=\" \
		--set-customization-variable OPEN_QUOTE_SYMBOL=\" \
		--html --css-include style.css \
		--set-customization-variable SHOW_TITLE=0 \
		--set-customization-variable DATE_IN_HEADER=1 \
		--output nncp.html index.texi
	cd doc && makeinfo -D "VERSION `perl -ne 'print "$$1\n" if /Version.* = "(.*)"$$/' < ../src/nncp.go`" \
		--set-customization-variable SECTION_NAME_IN_TITLE=1 \
		--set-customization-variable TREE_TRANSFORMATIONS=complete_tree_nodes_menus \
		--set-customization-variable CLOSE_QUOTE_SYMBOL=\" \
		--set-customization-variable OPEN_QUOTE_SYMBOL=\" \
		--output nncp.info index.texi


override_dh_installman:
	dh_installman
	for FILE in `cd debian/nncp/usr/bin && echo *`; do \
		ln -s nncp.1.gz "debian/nncp/usr/share/man/man1/$$FILE.1.gz"; done

override_dh_auto_clean:
	dh_auto_clean
	rm -rf debian/nncp.1 doc/*.info doc/*.plantuml.txt doc/*.html

# dwz generates errors for the go-built executables
override_dh_dwz:
	true

# Build was failing on 32-bit arm, timing out.
override_dh_auto_test:
	dh_auto_test -- -timeout 3600s
