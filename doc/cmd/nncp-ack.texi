@node nncp-ack
@pindex nncp-ack
@section nncp-ack

@example
$ nncp-ack [options] -all
$ nncp-ack [options] -node NODE[,@dots{}]
$ nncp-ack [options] -node NODE -pkt PKT

$ nncp-ack [@dots{}] 4>&1 >&2 | nncp-rm [@dots{}] -pkt
@end example

Send @ref{ACK, acknowledgement} of successful @option{PKT}
(Base32-encoded hash) packet receipt from @option{NODE} node. If no
@option{-pkt} is specified, then acknowledge all packet in node's
@code{rx} spool. If @option{-all} is specified, then do that for all
nodes.

That commands outputs list of created encrypted ACK packets
(@code{NODE/PKT}) to @strong{4}th file descriptor. That output can be
passed for example to @command{@ref{nncp-rm}} to remove them after
transmission to not wait for acknowledgement and retransmission.
