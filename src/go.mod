module go.cypherpunks.ru/nncp/v8

require (
	github.com/Arceliar/ironwood v0.0.0-20221115123222-ec61cea2f439
	github.com/davecgh/go-xdr v0.0.0-20161123171359-e6a2ba005892
	github.com/dustin/go-humanize v1.0.1
	github.com/flynn/noise v1.0.0
	github.com/fsnotify/fsnotify v1.6.0
	github.com/gologme/log v1.3.0
	github.com/gorhill/cronexpr v0.0.0-20180427100037-88b0669f7d75
	github.com/hjson/hjson-go v3.3.0+incompatible
	github.com/klauspost/compress v1.16.7
	github.com/yggdrasil-network/yggdrasil-go v0.4.7
	go.cypherpunks.ru/balloon v1.1.1
	go.cypherpunks.ru/recfile v0.7.0
	golang.org/x/crypto v0.12.0
	golang.org/x/net v0.10.0
	golang.org/x/sys v0.11.0
	golang.org/x/term v0.11.0
	gvisor.dev/gvisor v0.0.0-20230428223346-f33f75cda699
	lukechampine.com/blake3 v1.2.1
)

require (
	github.com/Arceliar/phony v0.0.0-20210209235338-dde1a8dca979 // indirect
	github.com/google/btree v1.0.1 // indirect
	github.com/klauspost/cpuid/v2 v2.2.5 // indirect
	golang.org/x/time v0.0.0-20220210224613-90d013bbcef8 // indirect
)

go 1.17
